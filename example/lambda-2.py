def soma(x, y):
    return x + y


def exec_func(call, *args):
    return call(*args)


print(exec_func(soma, 2, 3))
