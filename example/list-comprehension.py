list_simple = []

for i in range(10):
    list_simple.append(i)
print(list_simple)

list_complex = [i for i in range(324)]

print(list_complex)


product = [
    {'name': 'python', 'price': 13},
    {'name': 'JAVA', 'price': 22},
    {'name': 'Javascript', 'price': 33},
    {'name': 'Typescript', 'price': 3},
]

new_product = [
    {**product, 'price': product['price'] * 1.23}
    if product['price'] > 20 else {**product, 'price': product['price']}
    for product in product
]

print(*new_product, sep='\n')
