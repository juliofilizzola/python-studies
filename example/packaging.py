a, b = 1, 2
a, b = b, a

print(a, b)

person = {
    'name': 'joao',
    'lastname': 'silva'
}

a, b = person.values()

print(a, 'key a')
print(b, 'key b')


def desempacotar(*args, **kwargs):
    print('Não nomeados:', args)

    for k, v in kwargs.items():
        print(k, v)


desempacotar(**person)
